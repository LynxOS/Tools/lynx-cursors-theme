#!/usr/bin/env bash

ROOT_UID=0
DEST_DIR=

install_theme(){
  if [ -d "$DEST_DIR/Lynx-dark" ]; then
    rm -rf "$DEST_DIR/Lynx-dark"
  fi

  if [ -d "$DEST_DIR/Lynx" ]; then
    rm -rf "$DEST_DIR/Lynx"
  fi

  cp -r Lynx-dark/ $DEST_DIR/Lynx-cursors-dark
  cp -r Lynx/ $DEST_DIR/Lynx-cursors

  echo "Finished..."
}

# Destination directory
if [ "$UID" -eq "$ROOT_UID" ]; then
  DEST_DIR="/usr/share/icons"
else
  DEST_DIR="$HOME/.local/share/icons"
  mkdir -p $DEST_DIR
fi

while [ $# -gt 0 ]; do
  if [[ "$1" = "-d" ]]; then
    DEST_DIR="$2"
    shift
  fi
  shift
done

install_theme
